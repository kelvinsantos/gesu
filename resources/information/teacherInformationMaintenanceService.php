<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>
<?php

require_once(realpath(dirname(__FILE__) . "/../config.php"));

if (isset($_POST['accountId'])) {
		$accountId = $_POST['accountId'];
		if ($accountId != null && $accountId != "0") {
			
			$sql="UPDATE accounts SET email = '".$_POST['accountEmail']."', password = '".$_POST['accountPassword']."' 
				WHERE account_id = '".$_POST['accountId']."'";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			$sql="UPDATE teacher_information SET 
				id_number = '".$_POST['idNumber']."',
				full_name = '".$_POST['fullName']."',
				nick_name = '".$_POST['nickName']."',
				date_of_birth = '".$_POST['dateOfBirth']."',
				age_years = '".$_POST['years']."',
				age_months = '".$_POST['months']."',
				home_address = '".$_POST['homeAddress']."',
				phone = '".$_POST['phone']."'
				WHERE account_id = $accountId";
				
				if (!mysqli_query($mysqli,$sql)) {
					die('Error: ' . mysqli_error($mysqli));
				}
					
				mysqli_close($mysqli);
				
				$response = array(
					"message" => "Update success!",
					"action" => "update",
					"accountId" => $accountId
				);
				echo json_encode($response);

		} else {
			
			$sql="INSERT INTO accounts (email, password, role) VALUES ('".$_POST['accountEmail']."', '".$_POST['accountPassword']."', 'teacher')";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			$accountId = $mysqli->insert_id;
			
			$sql="INSERT INTO teacher_information (
				account_id, 
				id_number, 
				full_name, 
				nick_name, 
				date_of_birth,
				age_years,
				age_months, 
				home_address, 
				phone)
			VALUES
				(
				'".$accountId."', 
				'".$_POST['idNumber']."', 
				'".$_POST['fullName']."', 
				'".$_POST['nickName']."', 
				'".$_POST['dateOfBirth']."', 
				'".$_POST['years']."', 
				'".$_POST['months']."', 
				'".$_POST['homeAddress']."', 
				'".$_POST['phone']."')";
				
				if (!mysqli_query($mysqli,$sql)) {
					die('Error: ' . mysqli_error($mysqli));
				}
					
				mysqli_close($mysqli);
				
				$response = array(
					"message" => "Insert success!",
					"action" => "insert"
				);
				echo json_encode($response);
				
		}
	}

?>