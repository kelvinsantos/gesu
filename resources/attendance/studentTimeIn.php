<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$bootbox = "1";

	$title = "Student Time In";
	$useUpdateClock = true;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");

	$result = mysqli_query($mysqli,"SELECT * FROM student_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$_SESSION['accountId']."'");
	$row = mysqli_fetch_array($result)
?>

<script type="text/javascript">
$(function() {
	$("#save").click(function() {
		$.ajax({
		  method: "POST",
		  url: "studentTimeInService.php",
		  data: jQuery("form").serialize(),
		  success: function(response) {
		  		var response = $.parseJSON(response);
				bootbox.alert(response.message, function() {
					window.location.href='studentTimeIn.php';
				});
			}
		});
	});
});
</script>

<div class="container">
    <form method="POST" role="form" class="form-align">
        <h3><span class="label label-primary"><?php echo $title ?></span></h3>
        <br />
        <div class="form-group">
            <label>ID Number</label>
            <input type="text" class="form-control" name="idNumber" id="idNumber" value="<?php echo $row['id_number']; ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="fullName" id="fullName" value="<?php echo $row['full_name']; ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Date</label>
            <input type="text" class="form-control" name="date" id="date" value="<?php echo $date; ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Time In</label>
            <input type="text" class="form-control" name="timeIn" id="timeIn" value="<?php echo $current_date ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Teacher</label>
            <select class="form-control" name="teacher" id="teacher">
            <?php
			$result = mysqli_query($mysqli,"SELECT * FROM accounts AS a INNER JOIN teacher_information AS b ON a.account_id = b.account_id WHERE a.role='Teacher'");
			while($row = mysqli_fetch_array($result)) {
			?>
            <option value="<?php echo $row['full_name'] ?>"><?php echo $row['full_name'] ?></option>
            <?php } ?>
            </select>
        </div>
        <button type="button" class="btn btn-primary pull-right" name="save" id="save">Submit</button>
    </form>
</div>
<?php mysqli_close($mysqli); ?>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>